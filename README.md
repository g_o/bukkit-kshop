## kShop plugin

Virtual shop plugin that uses player as central bank and physical chests as storage (so that physical labor is made possible etc..).

### Bukkit page:
https://dev.bukkit.org/projects/kshop

### Discontinued
Feel free to fork and continue the project