package me.g_o.BukkitPlugin;

/**
 * BSD Licensed, check at bukkit.org.
 */

import java.util.Set;
import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class kShop extends JavaPlugin {

	static Player bank;
	static final Logger logger = Logger.getLogger("Minecraft");
	static Economy e;
	static float tax;
	static RegisteredServiceProvider<Economy> economyProvider;

	@Override
	public void onDisable() { //////////////////////////////DISABLE
		final PluginDescriptionFile pdfFile = this.getDescription();
		logger.info(pdfFile.getName() + " Has Been Disabled");
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onEnable() {//////////////////////////////ENABLE
		getConfig().options().copyDefaults(true);
		saveConfig();
		logger.info("Config file loaded successfuly. Set items and prices on config.yml!!!");
		final PluginDescriptionFile pdfFile = this.getDescription();
		economyProvider = Bukkit.getServer().getServicesManager()
				.getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			e = economyProvider.getProvider();
			logger.info(pdfFile.getName() + " V" + pdfFile.getVersion()
					+ " Has Been Enabled");
		} else{
			logger.severe("No economy provider! (economy provider could be iConomy for example)");
			onDisable();
			return;
		}
		
		bank = getServer().getPlayer(getConfig().getString("bank")); // Get
																		// player
																		// account
		if(bank==null) {
			logger.severe("[kShop] Invalid bank!");
			onDisable();
			return;
		}
		tax = Float.parseFloat(getConfig().getString("Tax"));
	}

	public boolean exist(final int id) {//////////////////////////////CHEST_EXIST
		return getConfig().contains("Chest" + id);
	}

	public Chest getChest(final int id) {//////////////////////////////GET_CHEST
		try {
		return (Chest) new Location(bank.getWorld(), getConfig().getInt("Chest" + id + ".x"),
				getConfig().getInt("Chest" + id + ".y"),
				getConfig().getInt("Chest" + id + ".z")).getBlock().getState(); // Getting
																				// Chest
		} catch (NullPointerException e){
			logger.severe("[kShop] Invalid chest in config file!");
		}
		return null;
	}

	public boolean pRespond(final CommandSender sender, final String msg) {//////////////////////////////SERVER_RESPOND
		sender.sendMessage(ChatColor.GREEN + msg);
		return true;
	}

	public boolean pError(final CommandSender sender, final String msg) {//////////////////////////////ERROR
		sender.sendMessage(ChatColor.RED + msg);
		return false;
	}

	boolean isFull(final Chest chest) {//////////////////////////////IS_CHEST_FULL
		final Inventory inv = chest.getInventory();
		// method a: if it contains empty slots
		boolean hasEmptySlot = false;
		for (final ItemStack stack : inv.getContents()) {
			if (stack == null) {
				hasEmptySlot = true;
				break;
			}
		}

		return hasEmptySlot;
	}
	
	//////////////////////////////PRINT PRICE
	public void printPrice(final String[] args, final CommandSender sender, final int i, int a) {
		float price = a*Float.parseFloat(getConfig().getString("prices." + args[i]));
		pRespond(sender, args[i] + "-" + price + "$ total-tax: " + kShop.tax * price + "$");
	}

	//////////////////////////////PAY
	public void pay(final Player a, final Player b, final int money) {
		e.depositPlayer(a.getName(), money);
		e.bankWithdraw(b.getName(), money);
	}
	
	/////////////////////////////COMMAND_HANDLER
	public boolean onCommand(final CommandSender sender, final Command cmd, final String commandLabel, final String[] args) {
		if (commandLabel.equalsIgnoreCase("price")) { // Price function
			if (args.length > 1 && args.length%2==0) {
				for (int i = 0; i < args.length; i+=2) {
					if (getConfig().contains("prices." + args[i])) {
						printPrice(args, sender, i, Integer.parseInt(args[i+1]));
					} else
						return pError(sender, "We don't sell/buy " + args[i]);
				}
			} else
				return pError(sender, "Too few arguments!");
		}

		if (commandLabel.equalsIgnoreCase("pricelist")) // Getting all products
		{
			final Set<String> ps = getConfig().getConfigurationSection("prices")
					.getKeys(false);
			final String[] pargs = (String[]) ps.toArray(new String[ps.size()]);
			for (int i = 0; i < pargs.length; i++)
				printPrice(pargs, sender, i, 1);
		}

		if (commandLabel.equalsIgnoreCase("buy") || commandLabel.equalsIgnoreCase("sell")) { // Trading with the bank.
			if (args.length >= 2) {
				int flag = 1;
				if (commandLabel.equalsIgnoreCase("buy"))
					flag = -1;
				if (commandLabel.equalsIgnoreCase("sell"))
					flag = 1;
				float money = 0;
				final Player player = (Player) sender;
				for (int i = 0; i < args.length; i += 2) {
					if (!getConfig().contains("prices." + args[i]))
						return pError(sender, "We don't sell " + args[i]);
					final ItemStack is = new ItemStack(
							Material.matchMaterial(args[i]),
							Integer.valueOf(args[i + 1]));
					money += Integer.valueOf(args[i + 1])
							* Float.valueOf((getConfig().getString("prices."
									+ args[i])));
					if (flag == -1) {
						if (e.getBalance(sender.getName()) > money) {
							Chest chest;
							int j = 0;
							do {
								if (!exist(j))
									return pError(
											sender,
											"Please contact an administrator, and report him that there isn't enough of this resource, or try a smaller amount!");
								chest = getChest(j);
								if(chest==null)
									return false;
								j++;
							} while (!chest.getInventory().containsAtLeast(is,
									Integer.valueOf(args[i + 1])));
							chest.getBlockInventory().removeItem(is); // remove from chest
							player.getInventory().addItem(is);
							e.bankWithdraw(sender.getName(), money * (1 + kShop.tax)); 
							e.depositPlayer(bank.getName(), money * (1 + kShop.tax));
						} else
							return pError(sender,
									"Sorry, you don't have enough money for that!");
					}
					if (flag == 1) {
						if (player.getInventory().containsAtLeast(is,
								Integer.valueOf(args[i + 1]))) {
							Chest chest;
							int j = 0;
							do {
								if (!exist(j))
									return pError(sender,
											"Please contact an administrator, and report him all chests are full!");
								if((chest = getChest(j))==null)
									return false;
								j++;
							} while (!isFull(chest));
							player.getInventory().removeItem(is);
							chest.getInventory().addItem(is); // add to chest
							e.depositPlayer(sender.getName(), money);
							e.bankWithdraw(bank.getName(), money);
						} else
							return pError(sender,
									"Sorry, you don't have that item stack on your inventory.");
					}
				}

				if (flag == -1)
					return pRespond(sender,
							"Thanks for buying these products with " + money
									* (1 + kShop.tax) + "$.");
				if (flag == 1)
					return pRespond(sender,
							"Thanks for selling these products for " + money
									+ "$.");
			} else
				return pError(sender, "Too few arguments!");
		}
		return true;
	}

}